import java.util.ArrayList;
import java.util.List;

public class Beast {

    String name;
    ArrayList<Food> favouriteFood = new ArrayList<>();

    public Beast(String name) {
        this.name = name;
    }

    public String dajGlos() {
        return "Sound of the Beast";
    }

    public void przedstawSie() {
        System.out.println(String.format("Nazywam sie %s.",this.name));
    }

    public String toString() {
        return String.format("Beast %s", name);
    }

    public void addFavFood(Food food) {
        if (favouriteFood.contains(food)) {
            food.increaseQuantityBy(food.quantity);
        } else
            favouriteFood.add(food);
    }

    public String arrayListFoodNamesToString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < favouriteFood.size(); i++) {
            sb.append(favouriteFood.get(i).getName());
            if (i != favouriteFood.size() - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    public String getQuantity() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < favouriteFood.size(); i++) {
            sb.append(this.favouriteFood.get(i).getName());
            sb.append(": ");
            sb.append(this.favouriteFood.get(i).quantity);

            if(i != favouriteFood.size() -1){
                sb.append(", ");
            }

        }
        return sb.toString();
    }
}

//        for (int i = 0;i<favouriteFood.size();i++){
//            if (favouriteFood.get(i).getName().equals(food.getName())){
//
//            }
//        }
//        favouriteFood.add(food);
//    }




public class Dog extends Beast implements Guaskable {

    public Dog(String name) {
        super(name);
    }

    public String dajGlos(){
        return "woof! woof!";
    }

    public String toString(){
        return String.format("Dog %s", name);
    }

    @Override
    public void glaskaj() {
        System.out.println("Pies jest głaskany");
    }
}

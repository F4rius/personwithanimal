public class Food {
    private namesOfFood name;
    double quantity = 0;

    public Food(namesOfFood name, double quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public String toString() {
        return name.toString();
    }

    public namesOfFood getName() {
        return name;
    }

    public void decreaseQuantityBy(double param) {
        this.quantity -= param;
    }

    public void increaseQuantityBy(double param) {
        this.quantity += param;
    }

}

public class Bird extends Beast implements Guaskable{

    public Bird(String name) {
        super(name);
    }

    @Override
    public String dajGlos() {
        return String.format("Ku ku, kra, kra!");
    }

    @Override
    public String toString() {
        return String.format("Ptak %s", name);
    }

    @Override
    public void glaskaj() {
        System.out.println("Ptak jest głaskany");
    }
}

public class Cat extends Beast implements Guaskable{

    public Cat(String name) {
        super(name);
    }

    public String dajGlos(){
        return "Meow!";
    }

    public String toString(){
        return String.format("Cat %s", name);
    }


    @Override
    public void glaskaj() {
        System.out.println("Kot jest głaskany :)");
    }
}

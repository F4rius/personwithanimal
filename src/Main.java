// napisz aplikacja ktora ma za zadanie stowrzyc osobe ktora bedzie mogla posiadac zwieraki roznego rodzaju, kazdy
// zwierzak ma swoja liste ulubionuch potraw osoba ma liste dostepnych potraw nakarm wszystkie zwierzaki


// 1. zaprojektuj klase jedzenie reprezentujaca rodzaj jedzenia i jego ilosc
// 2. zaprojektuj klase Kot Pies Ptak Ryba  ktore beda mialy liste swoich ulubionych potraw, dopisz metody dajGlos, przedawSie
// 3. zaprojektuj klase Osoba ktora bedzie miala liste zwierzakow jak rowniez liste dostepych potraw
// 4. napisz metode pozwalajaca nakarmic wszystkie posiadane przez Osobe Zwierzaki w zlaeznosci od ich upodobania
// 5. stworz odpowiednie obiekty i przetestuj dzialanie napisanej metody
// 6. napisz motode ktora pozwoli wykonac sztuczkę Zwierzakowi
// 7. rozbuduj metode nakarm tak aby w przypadku gdy zabraknie Osobie pokarmu dokupila w sklepie wystarczajaca ilosc
// tak aby mogla dokonczyc karmienie Zwierzaka

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        ArrayList<Beast> myBeasts = new ArrayList<>();

        myBeasts.add(new Fish("Nemo"));
        myBeasts.add(new Dog("Pluto"));
        myBeasts.add(new Cat("Filemon"));
        myBeasts.add(new Beast("Piękna"));

        ArrayList<Food> myFoods = new ArrayList<>();

        myFoods.add(new Food(namesOfFood.KLOPSIKI,5));
        myFoods.add(new Food(namesOfFood.MLEKO,2.5));
        myFoods.add(new Food(namesOfFood.BIGOS,50.5));


        for (int i = 0; i < myBeasts.size(); i++) {
            System.out.println(String.format("%-6s %8s:    %s",myBeasts.get(i).toString(),myBeasts.get(i).name,myBeasts.get(i).dajGlos()));

        }

        for (int i = 0; i < myFoods.size(); i++) {
            System.out.println(String.format(myFoods.get(i).toString()));

        }


    }
}

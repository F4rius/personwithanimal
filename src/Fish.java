public class Fish extends Beast {

    public Fish(String name) {
        super(name);
    }

    public String dajGlos(){
        return "Bul Ból Bool!";
    }

    public String toString(){
        return String.format("Fish %s", name);
    }
}
